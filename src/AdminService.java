import java.io.IOException;
import java.util.Scanner;

public class AdminService {

    private Admin admin;
    private AdminJSON adminJSON = new AdminJSON();


    public void enterAdmin() throws IOException {

       adminJSON.firstMethod();
       admin = adminJSON.admin;
        System.out.println("Enter Admin Login:");
        String login = new Scanner(System.in).nextLine();
        if (admin.checkLogin(login)){

            System.out.println("Enter Password For Admin:");
            int password = new Scanner(System.in).nextInt();
            if (admin.checkPassword(password)) {
                adminMenu();

            } else { System.out.println("The password does not match the login Admin");
                exitAdmin();
            }
        } else {
            System.out.println("This password does not have administrator rights!");
            System.out.println("You may login as a user, or try again.");
            System.out.println();
            exitAdmin();
        }
    }

    private void adminMenu() throws IOException {
        System.out.println("You are logged as an admin, select the following action:");
        System.out.println("1) ATM balance " + "\n" + "2) Refill ATM balance " + "\n" + "3) Create user"  + "\n" + "4) Exit admin mode");
        switch (new Scanner(System.in).nextInt()) {
            case 1:
                admin.balance();
                endMenu();
                break;
            case 2:
                admin.refillAtmBalance();
                endMenu();
                break;
            case 3:
                UserJSON userJSON = new UserJSON();
                userJSON.firstMethod();
                userJSON.addUser(admin.createUser());
                userJSON.writeUserJSON();
                System.out.println(userJSON.users.size());
                endMenu();
                break;
            case 4:
                exitAdmin();
                break;
            default:
                System.out.println("This option does not exist!");
                adminMenu();
                break;
        }
    }

    private void endMenu() throws IOException {
        System.out.println("You want to leave Admin mode:");
        System.out.println("1) NO" + "\n" + "2) YES");
        switch (new Scanner(System.in).nextInt()) {
            case 1:
                adminMenu();
                break;
            case 2:
                exitAdmin();
                break;
            default:
                System.out.println("This option does not exist!");
                System.out.println();
                endMenu();
                break;
        }
    }

    private void exitAdmin() throws IOException {
        adminJSON.writeAdminJSON(admin);
        System.out.println("Thank you for using admin mode!");
        System.out.println();
        Main.startMenu();
    }





}
