import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;

public class AdminJSON {
    private static String link = "E:\\_My\\_Programming\\Java_Project\\_ATM_all_version\\ATM_v2.0-JSON\\admin.json";
    private static File file = new File(link);
    public static Admin admin;
    public Integer atmBalance=openAtmBalansceForUser();

    public void firstMethod()  throws IOException {
        if (file.exists()&&!file.isDirectory()) {
            admin = readAdminJSON();
        } else {
            Admin admin = new Admin();
            admin.setLogin("admin");
            admin.setPassword(1234);
            admin.setBalance(100000);
            writeAdminJSON(admin);
            firstMethod();
        }
    }

    public Integer openAtmBalansceForUser() {
        if (file.exists()&&!file.isDirectory()) {
            admin = readAdminJSON();
            atmBalance = admin.getBalance();
            return atmBalance;
        } else {
            System.out.println("");
            return 0;
        }
    }

      public static void writeAdminJSON(Admin admin) throws IOException {
        JsonFactory jfactory = new JsonFactory();
        JsonGenerator jsonGenerator = jfactory.createGenerator(new File(link), JsonEncoding.UTF8);
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(jsonGenerator,admin);
        }
        catch (JsonMappingException e){
            e.printStackTrace();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static Admin readAdminJSON() {
        ObjectMapper mapper = new ObjectMapper();
        try {
           admin = mapper.readValue(new File(link), Admin.class);

        } catch (JsonMappingException e){
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return admin;
    }
}
