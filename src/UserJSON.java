import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class UserJSON {

    private static String link = "resources\\user.json";
    private static File file = new File(link);
    public static List<User> users = new ArrayList<>();

    public void addUser(User user) {
        users.add(user);
    }


    public void firstMethod()  throws IOException {
        if (file.exists()&&!file.isDirectory()) {
            users = readUserJSON();
        } else {
            writeUserJSON();
            User user = new User();
            user.setLogin("user");
            user.setPassword(0000);
            addUser(user);
            firstMethod();
        }
    }

    public static void writeUserJSON() throws IOException {
        JsonFactory jfactory = new JsonFactory();
        JsonGenerator jsonGenerator = jfactory.createGenerator(new File(link), JsonEncoding.UTF8);
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(jsonGenerator,users);
        }
        catch (JsonMappingException e){
            e.printStackTrace();
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static List<User> readUserJSON() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            users = mapper.readValue(new File(link), new TypeReference<List<User>>(){});

        } catch (JsonMappingException e){
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return users;
    }
}
