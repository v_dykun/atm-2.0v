import java.util.InputMismatchException;
import java.util.Scanner;

public class Admin extends AbstractUser {

    @Override
    boolean checkLogin(String login) {
        if (login.equals(getLogin())) {
            return true;
        } else return false;
        }

    @Override
    boolean checkPassword(Integer password) {
        if (password.equals(getPassword())) {
            return true;
        } else return false;
    }

    @Override
    void balance() {
        System.out.println("ATM balance is: "+ getBalance());
    }


    public void refillAtmBalance() {
        try {
            System.out.println("Enter the amount of replenishment of the balance: ");
            int ball = new Scanner(System.in).nextInt();
            int atmBalance = getBalance() + ball;
            setBalance(atmBalance);

            System.out.println("New balance is: " + atmBalance);
        } catch (InputMismatchException e) {
            System.out.println("The amount should consist only of digits.");
            System.out.println();
            refillAtmBalance();
        }
    }


    public User createUser() {
        User user = new User();
        try {
            System.out.println("Enter user Login:");
            user.setLogin(new Scanner(System.in).nextLine());
            System.out.println("Enter the password for user Login:");
            user.setPassword(new Scanner(System.in).nextInt());
            user.setBalance(0);
            System.out.println("User is create!");
        } catch (InputMismatchException e) {
            System.out.println("The password must consist of digits.");
            System.out.println("User not created. Start from the beginning.");
            System.out.println();
            createUser();
        }
        return user;
    }
}
