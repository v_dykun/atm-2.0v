import java.io.IOException;
import java.util.Scanner;

public class UserServise {
    private AdminJSON adminJSON = new AdminJSON();
    private UserJSON userJSON = new UserJSON();
    private Integer atmBalance = adminJSON.atmBalance;
    private User user;

    public void enterUser() throws IOException {
        int a=0;
        userJSON.firstMethod();

        System.out.println("Enter User Login:");
        String login = new Scanner(System.in).nextLine();

    for (int i=0;i<userJSON.users.size();i++) {
        if (userJSON.users.get(i).getLogin().equals(login)) {
            a++;
            System.out.println("Enter Password For this User:");
            Integer password = new Scanner(System.in).nextInt();
            if (userJSON.users.get(i).getPassword().equals(password)) {
                user=userJSON.users.get(i);
                userMenu();
            } else {
                System.out.println("The password does not match the login "+login);
                exitUser();
            }
        }
    }

        if (a ==0) {
            System.out.println("This user is not in the database.");
            System.out.println("Contact the bank and become our client");
            exitUser();
        }


    }

    private void userMenu () throws IOException {
        System.out.println();
        System.out.println("You are logged in as an "+user.getLogin() +", select the following action:");
        System.out.println("1) You balance " + "\n" + "2) Put Cash " + "\n" + "3) Take Cash"  + "\n" + "4) Exit user mode");
        switch (new Scanner(System.in).nextInt()) {
            case 1:
                user.balance();
                endMenu();
                break;
            case 2:
                putCash();
                endMenu();
                break;
            case 3:
                takeCash();
                endMenu();
                break;
            case 4:
                exitUser();
                break;
            default:
                System.out.println("This option does not exist!");
                break;
        }
    }

    private void putCash() {
        System.out.println("Specify the amount you want to put: ");
        int cash = new Scanner(System.in).nextInt();
        user.setBalance(user.getBalance()+cash);
        atmBalance = atmBalance +cash;
        user.balance();
    }

    private void takeCash() {
        System.out.println("Specify the amount you want to take: ");
        int cash = new Scanner(System.in).nextInt();
            if (user.getBalance()>cash) {
                if (atmBalance>cash) {
                    user.setBalance(user.getBalance()-cash);
                    atmBalance=atmBalance-cash;
                    user.balance();
                } else {
                    System.out.println("ATM is not enough cash.");
                    System.out.println("At the ATM account there is: "+atmBalance);
                    System.out.println("The nearest ATM is located on the street Zelena, 150");
                    System.out.println();
                }
            } else {
                System.out.println("You do not have enough funds in your account!");
                System.out.println("Please, refill your account!");
                System.out.println();
            }
    }

    private void endMenu() throws IOException {
        System.out.println("You want to leave User mode:");
        System.out.println("1) NO" + "\n" + "2) YES");
        switch (new Scanner(System.in).nextInt()) {
            case 1:
                userMenu();
                break;
            case 2:
                exitUser();
                break;
            default:
                System.out.println("This option does not exist!");
                endMenu();
                break;
        }
    }

    private void exitUser () throws IOException {
        adminJSON.admin.setBalance(atmBalance);
        adminJSON.writeAdminJSON(adminJSON.admin);
        System.out.println("Thank you for using ATM");
        System.out.println();
        Main.startMenu();
    }

}
