public class User extends AbstractUser {

    public User() {
    }

    @Override
    boolean checkLogin(String login) {
        if (login.equals(getLogin())) {
            return true;
        } else return false;
    }

    @Override
    boolean checkPassword(Integer password) {
        if (password.equals(getPassword())) {
            return true;
        } else return false;
    }

    @Override
    void balance() {
        System.out.println("Your balance:"+ getBalance());
    }

}
