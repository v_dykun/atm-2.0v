import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main{
    public static void main(String... args) throws IOException {
        try {
            startMenu();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void startMenu() throws IOException {
        try {
            System.out.println("Welcome to ATM. Make your choice:");
            System.out.println("1) Admin service " + "\n" + "2) Client service " + "\n" + "3) Exit ");
            switch (new Scanner(System.in).nextInt()) {
                case 1:
                    AdminService adminService = new AdminService();
                    adminService.enterAdmin();
                    break;
                case 2:
                    UserServise userServise = new UserServise();
                    userServise.enterUser();
                    break;
                case 3:
                    exit();
                    break;
                default:
                    System.out.println("This option does not exist!");
                    System.out.println();
                    startMenu();
                    break;
            }

        } catch (InputMismatchException e) {
            System.out.println("Don't enter latter!");
            System.out.println();
            startMenu();
        }

    }


    private static void exit() {
        System.out.println("Thank you for using ATM");
    }
}

